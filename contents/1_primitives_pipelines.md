## Primitives and Pipelines

Let's start with basic definitions in order for us to understand a little bit
better what happens when we run a pipeline later in the tutorial.

A *pipeline* is basically a series of steps that are executed in order to solve
a particular *problem* (such as prediction based on historical data). A step of
a pipeline is usually a *primitive* (a step can be something else, however, like
a sub-pipeline, but for the purposes of this tutorial, assume that each step is
a primitive): something that individually could, for example, read the dataset,
or transform data into another format, or fit a model for prediction. There are
many types of primitives (see the `primitives` index repo in the Public Code
link above). In a pipeline, the steps must be arranged in a way such that each
step must be able to read the data in the format produced by the preceding step.

For this tutorial, let's try to use the sample pipeline that comes with a
primitive called `d3m.primitives.classification.logistic_regression.SKlearn` in
order to predict baseball hall-of-fame players, based on their stats (see the
`185_baseball` dataset from the Public or Internal datasets link above
(`datasets/seed_datasets_current/185_baseball`).

Let's take a look at the sample pipeline. At the time of this writing, it can be
found in the `primitives` index repo
[here](https://gitlab.com/datadrivendiscovery/primitives/blob/master/v2020.1.9/JPL/d3m.primitives.classification.logistic_regression.SKlearn/2019.11.13/pipelines/862df0a2-2f87-450d-a6bd-24e9269a8ba6.json),
but this repo's directory names and files periodically change, so it is prudent
to see how to navigate to this file too. From the Public Code link above, go to:
- `primitives`
- `v2020.1.9` (version of the primitive index, changes periodically)
- `JPL` (the organization that develops/maintains the
  primitive)
- `d3m.primitives.classification.logistic_regression.SKlearn` (the python path of the actual primitive)
- `2019.11.13` (the version of this primitive, changes periodically)
- `pipelines`
- `862df0a2-2f87-450d-a6bd-24e9269a8ba6.json` (actual pipeline description filename, changes periodically)

Early on this JSON document, you will see a list called `steps`. This is the
actual list of primitive steps that run one after another in a pipeline. Each
step has the information about the primitive, as well as arguments, outputs, and
hyperparameters, if any. This specific pipeline has 5 steps (the
`d3m.primitives` prefix is omitted in the following list):
- `data_transformation.dataset_to_dataframe.Common`
- `data_transformation.column_parser.Common`
- `data_cleaning.imputer.SKlearn`
- `classification.logistic_regression.SKlearn`
- `data_transformation.construct_predictions.Common`

Now let's take a look at the first primitive step in that pipeline. We can find
the source code of this primitive in the common-primitives repo
[(common-primitives/common_primitives/dataset_to_dataframe.py)](https://gitlab.com/datadrivendiscovery/common-primitives/blob/master/common_primitives/dataset_to_dataframe.py).
Take a look particularly at the `produce` method. This is essentially what the
primitive does. Try to do this for the other primitive steps in the pipeline as
well - take a cursory look at what each one essentially does (note that for the
actual classifier primitive, you should look at the `fit` method as well to see
how the model is trained). Primitives whose python path suffix is `*.Common` is
in the `common-primitives` repo, and those that have a `*.SKlearn` suffix is in
the `sklearn-wrap` repo (checkout the `dev-dist` branch). If you're having a
hard time looking for the correct source file, you can try taking the primitive
id from the primitive step description in the pipeline, and `grep` for it inside
the repo.
