## TA1 Primitive Development - 1: Primitive

Before building any primitives, it is important to set up a Github Project repository as shown in
examples below:
 - https://github.com/tonyjo/d3m_test

**Note:** All the explainations below will be based on the [d3m_test](https://github.com/tonyjo/d3m_test) setup.

### Overview of building a primitive

1. When bulding a primitive, it is important to recognize what kind of primitive, based on
the available [primitive_interfaces](https://gitlab.com/datadrivendiscovery/d3m/tree/devel/d3m/primitive_interfaces).
2. The next step is to identify the input and output [container](https://gitlab.com/datadrivendiscovery/d3m/tree/devel/d3m/container) types.
3. Setup [metadata](https://gitlab.com/datadrivendiscovery/d3m/tree/devel/d3m/metadata) for each primitive.
4. Write a Unit Test to verify the primitive functions.
5. Generate the `json` file description for the primitive.


#### 1. Primitive Kind
There are a variety of primitve interfaces available. As an example, just to have feature extraction without any fitting required, meaning to produce useful outputs (features) from inputs (data) directly, ``TransformerPrimitiveBase`` from `` transformer.py``can be used.

Each primitives can have it's own [hyperparameters](https://gitlab.com/datadrivendiscovery/d3m/blob/devel/d3m/metadata/hyperparams.py). Some example types are:
```
Constant, UniformBool, UniformInt, Choice, List
```
Also, each hyperparameters should be defined as one of the four [semantic types](https://docs.datadrivendiscovery.org/v2020.1.9/metadata.html#hyper-parameters):
```
https://metadata.datadrivendiscovery.org/types/TuningParameter
https://metadata.datadrivendiscovery.org/types/ControlParameter
https://metadata.datadrivendiscovery.org/types/ResourcesUseParameter
https://metadata.datadrivendiscovery.org/types/MetafeatureParameter
```

**Sample Setup**
```python
from d3m.primitive_interfaces import base, transformer
from d3m.metadata import base as metadata_base, hyperparams

__all__ = ('SampleTransform',)

class Hyperparams(hyperparams.Hyperparams):
  """
  docstrings
  """
  learning_rate = hyperparams.Uniform(lower=0.0, upper=1.0, default=0.001, semantic_types=[
      'https://metadata.datadrivendiscovery.org/types/TuningParameter'
  ])
  clusters = hyperparams.UniformInt(lower=1, upper=100, default=10, semantic_types=[
      'https://metadata.datadrivendiscovery.org/types/TuningParameter'
  ])

class SampleTransform(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
  """
  docstrings
  """
  def __init__(self, *, hyperparams: Hyperparams, volumes: typing.Union[typing.Dict[str, str], None]=None):
    super().__init__(hyperparams=hyperparams, volumes=volumes)
    self.hyperparams = hyperparams

  def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
    pass
```

#### 2. Input/Output type
The acceptable input/outputs must be pre-defined. D3M supports a variety of standard IO's such as:
- DataFrame  :`pandas.DataFrame`
- ndarray    :`numpy.ndarray`
- matrix     :`numpy.matrix`
- List       :`list`

**Sample Setup**

```python
from d3m import container

Inputs  = container.DataFrame
Outputs = container.DataFrame


class SampleTransform(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    docstrings
    """
    __author__ = <Your-Name>
    metadata   = ...

    def __init__(self, *, hyperparams: Hyperparams, volumes: typing.Union[typing.Dict[str, str], None]=None):
        super().__init__(hyperparams=hyperparams, volumes=volumes)
       ....
    ...
```

#### 3. Metadata

It is very crucial to set up the metadata for the primitive properly. D3M uses the primitive's metadata to index and run TA-2 systems.

Below is a description of core metadata setup required for each primitive.

**Sample Setup**

```python
from d3m.primitive_interfaces import base, transformer
from d3m.metadata import base as metadata_base, hyperparams

__all__ = ('SampleTransform',)

class SampleTransform(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    docstrings
    """
    __author__ = <Your-Name>
    metadata = metadata_base.PrimitiveMetadata({
        'id': <Unique-ID, generated using UUID>,
        'version': <Primitive-development-version>,
        'name': <Primitive-Name>,
        'python_path': 'd3m.primitives.<>.<>' # Must match path in setup.py,
        'source': {
            'name': <Project-maintainer-name>,
            'uris': [<GitHub-link-to-project>],
            'contact': 'mailto:<Author E-Mail>'
        },
        'installation': [{
            'type': metadata_base.PrimitiveInstallationType.PIP,

            'package_uri': 'git+<GitHub-link-to-project>@{git_commit}#egg=<Project_Name>'.format(
                git_commit=d3m_utils.current_git_commit(os.path.dirname(__file__)),
            ),
        }],
        'algorithm_types': [
            metadata_base.PrimitiveAlgorithmType.<Choose-the-algorithm-type-that-best-describes-the-primitive>,
            # Check https://metadata.datadrivendiscovery.org/devel/?definitions#definitions.algorithm_types for all available algorithm types.
        ],
        'primitive_family': metadata_base.PrimitiveFamily.<Choose-the-primitive-familty-that-closely-associates-to-the-primitive>
        # Check https://metadata.datadrivendiscovery.org/devel/?definitions#definitions.primitive_family for all available primitive family types.
    })

    def __init__(self, *, hyperparams: Hyperparams, volumes: typing.Union[typing.Dict[str, str], None]=None):
      ....
```

#### 4. Unit Test
Once the primitives are constructed, unit testing must be done to see if the primitive works as intended.

**Sample Setup**

```python
import os
import unittest

from d3m.container.dataset import Dataset
from d3m.metadata import base as metadata_base
from common_primitives import dataset_to_dataframe

from sample_primitive import SampleTransform

class TestSampleTransform(unittest.TestCase):
  """
  docstrings
  """
  def test():
    # Load Some dataset,
    # Datasets can be obtained from: https://gitlab.datadrivendiscovery.org/d3m/datasets
    base_path = '.../datasets/seed_datasets_current/'
    dataset_doc_path = os.path.join(base_path, '38_sick_dataset', 'datasetDoc.json')
    dataset = Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))
    dataframe_hyperparams_class = dataset_to_dataframe.DatasetToDataFramePrimitive.metadata.get_hyperparams()
    dataframe_primitive = dataset_to_dataframe.DatasetToDataFramePrimitive(hyperparams=dataframe_hyperparams_class.defaults())
    dataframe = dataframe_primitive.produce(inputs=dataset)

    # Call Primitive
    hyperparams_class = SampleTransform.metadata.get_hyperparams()
    primitive  = SampleTransform(hyperparams=hyperparams_class.defaults(), volumes=all_volumes)
    test_out   = primitive.produce(inputs=dataframe.value)

    # Write Checks to make sure that the output (type and shape) is whats Expected
    assert()

    ...

if __name__ == '__main__':
  unittest.main()

```

It is recommended to do the testing inside the D3M docker container:

```shell
docker run  -v ../local/path/d3m_test:/d3m_test
            -i -t registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9\
            /bin/bash

cd /d3m_test/samplePrimitive1

python3 primitive_name_test.py
```

#### 5. Primitive `json` file description
Once primitive is constructed and unit testing is successfully completed, the final step in building a
primitve is to generate the `json` file description which will be indexed and used by D3M.

```shell
docker run  --rm \
            -v ../local/path/d3m_test:/d3m_test\
            -i -t registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9\
            /bin/bash

cd /d3m_test

pip3 install -e .

cd /samplePrimitive1

python3 -m d3m index describe -i 4 <primitive_name>
```

or a [python file](https://github.com/tonyjo/d3m_test/blob/master/samplePrimitives/generate-primitive-json.py) can be written to generate the `json` file description as well. This would be more convenient when having to manage multiple primitives. In this case, generating the `json` primitive is shown as follows:
```shell
docker run  --rm \
            -v ../local/path/d3m_test:/d3m_test
            -i -t registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9\
            /bin/bash

cd /d3m_test

pip3 install -e .

python3 generate-primitive-json.py --args..
```
