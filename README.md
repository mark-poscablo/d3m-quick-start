# D3M-TA1 Quick Start Guide

This document aims to be a tutorial, or a quick-start guide, for newcomers to
the D3M TA1 project. It is not meant to be a comprehensive guide to everything
about D3M, or even just TA1. The goal here is for the reader to be able to write
a new, simple, but working primitive by the end of this tutorial. To achieve
this goal, this tutorial is divided into several sections:

## Table of Contents

- [Important links](#important-links)
    - [Public](#public)
    - [Internal](#internal)
- [Overview of primitives and pipelines](#overview-of-primitives-and-pipelines)
- [Setting up a local d3m environment](#setting-up-a-local-d3m-environment)
- [Running a sample pipeline](#running-a-sample-pipeline)
    - [Preparing the dataset](#preparing-the-dataset)
    - [Specifying all the necessary paths of a dataset](#specifying-all-the-necessary-paths-of-a-dataset)
    - [Using a pipeline run output file](#using-a-pipeline-run-output-file)
- [Writing a new primitive](#writing-a-new-primitive)
    - [Primitive source code](#primitive-source-code)
    - [setup.py](#setuppy)
    - [Primitive unit tests](#primitive-unit-tests)
    - [Using this primitive in a pipeline](#using-this-primitive-in-a-pipeline)
- [Beyond this tutorial](#beyond-this-tutorial)
- [Quick reference to writing your own primitives and pipelines](#quick-reference-to-writing-your-own-primitives-and-pipelines)
    - [TA1 Primitive Development - 1: Primitive](#ta1-primitive-development-1-primitive)
        - [Overview of building a primitive](#overview-of-building-a-primitive)
        - [1. Primitive Kind](#1-primitive-kind)
        - [2. Input/Output type](#2-inputoutput-type)
        - [3. Metadata](#3-metadata)
        - [4. Unit Test](#4-unit-test)
        - [5. Primitive `json` file description](#5-primitive-json-file-description)
    - [TA1 Primitive Development - 2: Pipeline](#ta1-primitive-development-2-pipeline)
        - [1. Import all the necessary packages](#1-import-all-the-necessary-packages)
        - [2. Primitive step construction](#2-primitive-step-construction)
        - [3. Run and Validate `pipeline`](#3-run-and-validate-pipeline)

**Note:** The d3m core package version is `v2020.1.9` at the time of this
writing

## Important links

First, here is a list of some important links that should help you with
reference and instructional material beyond this quick start guide. Be aware
also that the d3m core source code has extensive docstrings that you may find
helpful (the `d3m` repo can be accessed from the Public Code link below).

### Public
- Docs:                https://docs.datadrivendiscovery.org/
- Code:                https://gitlab.com/datadrivendiscovery
- Datasets:            https://datasets.datadrivendiscovery.org/d3m/datasets
- Docker images:       https://docs.datadrivendiscovery.org/docker.html
- TA1, TA2, TA3 repos: https://github.com/darpa-i2o/d3m-program-index


### Internal
- Wiki:     https://datadrivendiscovery.org/wiki/#all-updates
- Code:     https://gitlab.datadrivendiscovery.org/explore/groups
- Datasets: https://gitlab.datadrivendiscovery.org/d3m/datasets

[back to top](#table-of-contents)

## Overview of primitives and pipelines

Let's start with basic definitions in order for us to understand a little bit
better what happens when we run a pipeline later in the tutorial.

A *pipeline* is basically a series of steps that are executed in order to solve
a particular *problem* (such as prediction based on historical data). A step of
a pipeline is usually a *primitive* (a step can be something else, however, like
a sub-pipeline, but for the purposes of this tutorial, assume that each step is
a primitive): something that individually could, for example, read the dataset,
or transform data into another format, or fit a model for prediction. There are
many types of primitives (see the `primitives` index repo in the Public Code
link above). In a pipeline, the steps must be arranged in a way such that each
step must be able to read the data in the format produced by the preceding step.

For this tutorial, let's try to use the sample pipeline that comes with a
primitive called `d3m.primitives.classification.logistic_regression.SKlearn` in
order to predict baseball hall-of-fame players, based on their stats (see the
`185_baseball` dataset from the Public or Internal datasets link above
(`datasets/seed_datasets_current/185_baseball`).

Let's take a look at the sample pipeline. At the time of this writing, it can be
found in the `primitives` index repo
[here](https://gitlab.com/datadrivendiscovery/primitives/blob/master/v2020.1.9/JPL/d3m.primitives.classification.logistic_regression.SKlearn/2019.11.13/pipelines/862df0a2-2f87-450d-a6bd-24e9269a8ba6.json),
but this repo's directory names and files periodically change, so it is prudent
to see how to navigate to this file too. From the Public Code link above, go to:
- `primitives`
- `v2020.1.9` (version of the primitive index, changes periodically)
- `JPL` (the organization that develops/maintains the
  primitive)
- `d3m.primitives.classification.logistic_regression.SKlearn` (the python path
  of the actual primitive)
- `2019.11.13` (the version of this primitive, changes periodically)
- `pipelines`
- `862df0a2-2f87-450d-a6bd-24e9269a8ba6.json` (actual pipeline description
  filename, changes periodically)

Early on this JSON document, you will see a list called `steps`. This is the
actual list of primitive steps that run one after another in a pipeline. Each
step has the information about the primitive, as well as arguments, outputs, and
hyperparameters, if any. This specific pipeline has 5 steps (the
`d3m.primitives` prefix is omitted in the following list):
- `data_transformation.dataset_to_dataframe.Common`
- `data_transformation.column_parser.Common`
- `data_cleaning.imputer.SKlearn`
- `classification.logistic_regression.SKlearn`
- `data_transformation.construct_predictions.Common`

Now let's take a look at the first primitive step in that pipeline. We can find
the source code of this primitive in the common-primitives repo
[(common-primitives/common_primitives/dataset_to_dataframe.py)](https://gitlab.com/datadrivendiscovery/common-primitives/blob/master/common_primitives/dataset_to_dataframe.py).
Take a look particularly at the `produce` method. This is essentially what the
primitive does. Try to do this for the other primitive steps in the pipeline as
well - take a cursory look at what each one essentially does (note that for the
actual classifier primitive, you should look at the `fit` method as well to see
how the model is trained). Primitives whose python path suffix is `*.Common` is
in the `common-primitives` repo, and those that have a `*.SKlearn` suffix is in
the `sklearn-wrap` repo (checkout the `dev-dist` branch). If you're having a
hard time looking for the correct source file, you can try taking the primitive
`id` from the primitive step description in the pipeline, and `grep` for it
inside the repo. For example, if you were looking for the source code of the
first primitive step in this pipeline, first look at the primitive info in that
step and get its `id`:

```json
"primitive": {
  "id": "4b42ce1e-9b98-4a25-b68e-fad13311eb65",
  "version": "0.3.0",
  "python_path": "d3m.primitives.data_transformation.dataset_to_dataframe.Common",
  "name": "Extract a DataFrame from a Dataset"
},
```

Then, run this:

```shell
git clone https://gitlab.com/datadrivendiscovery/common-primitives.git
cd common-primitives
grep -r 4b42ce1e-9b98-4a25-b68e-fad13311eb65 . | grep -F .py
```

However, this series of commands assumes that you know exactly which specific
repository is the primitive's source code located in (the `git clone` command).
Since this is probably not the case for an arbitrarily given primitive, there is
a method on how to find out the repository URL of any primitive, and it requires
using a d3m docker image, which is described in the next section.

[back to top](#table-of-contents)

## Setting up a local d3m environment

In order to run a pipeline, you must have a Python environment where the d3m
core package is installed, as well as the packages of the primitives installed
as well. While it is possible to setup a Python virtual environment and install
the packages them through `pip`, in this tutorial, we're going to use the D3M
docker images instead (in many cases, even beyond this tutorial, this will save
you a lot of time and effort trying to find the any missing primitive packages,
manually installing them, and troubleshooting installation errors). So, make
sure [Docker](https://docs.docker.com/) is installed in your system.

You can find the list of D3M docker images in the Public Docker images link
above. The one we're going to use in this tutorial is the v2020.1.9 primitives
image (feel free to use whatever the latest one instead though - just modify the
`v2020.1.9` part accordingly):

```shell
docker pull registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9
```

Once you have downloaded the image, we can finally run the d3m modules (and
hence run a pipeline). Before running a pipeline though, let's first try to get
a list of what primitives are installed in the image's Python environment:

```shell
docker run --rm registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9 python3 -m d3m index search
```

You should get a big list of primitives. All of the known primitives to D3M
should be there.

The previous section mentions a method of determining where the source code of
an arbitrarily given primitive can be found. We can do this using the d3m python
module within a d3m docker container. First get the `python_path` of the
primitive step (see the JSON snippet above of the primitive's info from the
pipeline). Then, run this command:

```shell
docker run --rm registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9 python3 -m d3m index describe d3m.primitives.data_transformation.dataset_to_dataframe.Common
```

Near the top of the huge JSON string that it returns, you'll see `"source"`, and
inside it, `"uris"`. This should give the URI of the git repo where the source
code of that primitive can be found. Also, You can also substitute the primitive
`id` for the `python_path` in that command, but the command usually returns a
result faster if you provide the `python_path`. Note also that you can only do
this for primitives that have been submitted for a particular image (primitives
that are contained in the d3m `primitives` repo, found in the Public Code link
above).

It can be obscure at first how to use the d3m python module, but you can always
access the help string for each d3m command at every level of the command chain
by using the `-h` flag. This is useful especially for the getting a list of all
the possible arguments for the `runtime` module.

```shell
docker run --rm registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9 python3 -m d3m -h
docker run --rm registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9 python3 -m d3m index -h
docker run --rm registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9 python3 -m d3m runtime -h
docker run --rm registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9 python3 -m d3m runtime fit-score -h
```

One last point before we try running a pipeline. The docker container must be
able to access the dataset location and the pipeline location from the host
filesystem. We can do this by
[bind-mounting](https://docs.docker.com/storage/bind-mounts/) a host directory
that contains both the `datasets` repo and the `primitives` index repo to a
container directory. Git clone these repos (they can be found in the Public Code
link above), and also make another empty directory called `pipeline-outputs`.
Now, if your directory structure looks like this:

```
/home/foo/d3m
├── datasets
├── pipeline-outputs
└── primitives
```

Then you'll want to bind-mount `/home/foo/d3m` to a directory in the container,
say `/mnt/d3m`. You can specify this mapping in the docker command itself:

```shell
docker run \
    --rm \
    -v /home/foo/d3m:/mnt/d3m \
    registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2019.11.10 \
    ls /mnt/d3m
```

If you're reading this tutorial from a text editor, it might be a good idea at
this point to find and replace `/home/foo/d3m` with the actual path in your
system where the `datasets`, `pipeline-outputs`, and `primitives` directories
are all located. This will make it easier for you to just copy and paste the
commands from here on out, instead of changing the faux path every time.

[back to top](#table-of-contents)

## Running a sample pipeline

At this point, let's try running a pipeline. Again, we're going to run the
sample pipeline that comes with
`d3m.primitives.classification.logistic_regression.SKlearn`. There are two ways
to run a pipeline: by specifying all the necessary paths of the dataset, or by
specifying and using a pipeline run output file. Let's make sure first though
that the dataset is usable, as described in the next subsection.

### Preparing the dataset

Towards the end of the previous section, you were asked to git clone the
`datasets` repo (whether it's the public or internal version does not matter for
this exercise) in your machine. Most likely, you might have accomplished that
like this:

```shell
git clone https://datasets.datadrivendiscovery.org/d3m/datasets.git
```

If, however, you used git LFS to download the repo like in the following command
(and as described too in the README of the repo), you can skip this step and
move on to the next subsection.

```shell
git lfs clone https://datasets.datadrivendiscovery.org/d3m/datasets.git
```

The second command downloads the entire contents of the repo to your machine
through git LFS (download and install [git
LFS](https://github.com/git-lfs/git-lfs) if you haven't yet). The first one
might not. The repo is organized such that all files larger than 100 KB is
stored in git LFS. Thus, if you used the first command, you most likely have to
do a one-time extra step before you can use a dataset, as some files of that
dataset that are over 100 KB will not have the actual data in them (although
they will still exist as files in the cloned repo). This is true even for the
dataset that we will use in this exercise, `185_baseball`. To verify this, open
this file in a text editor:

```
datasets/seed_datasets_current/185_baseball/185_baseball_dataset/tables/learningData.csv`
```

Then, see if it contains text similar to this:

```
version https://git-lfs.github.com/spec/v1
oid sha256:931943cc4a675ee3f46be945becb47f53e4297ec3e470c4e3e1f1db66ad3b8d6
size 131187
```

If it does, then this dataset has not yet been fully downloaded from git LFS
(but if it looks like a normal CSV file, then you can skip the rest of this
subsection and move on). To download this dataset, simply run this command
inside the `datasets` directory:

```shell
git lfs pull -I seed_datasets_current/185_baseball/
```

Inspect the file again, and you should see that it looks like a normal CSV file
now.

In general, if you don't know which specific dataset does a certain sample
pipeline in the `primitives` repo uses, inspect the pipeline run output file of
that primitive (whose file path is similar to that of the pipeline JSON file, as
described in the "[Overview of primitives and
pipelines](#overview-of-primitives-and-pipelines)" section), but instead of
going to `pipelines`, go to `pipeline_runs`). The pipeline run output is
initially gzipped in the `primitives` repo, so decompress it first. Then open up
the actual .yml file, look at `problem`, and under it should be `id`. If you do
that for the sample pipeline run output of the SKlearn logistic regression
primitive that we're looking at for this exercise, you'll find that the problem
id is `185_baseball_problem`. The name of the dataset is this string, without
the `_problem` part.

Now, let's actually run the pipeline using the two ways mentioned earlier.

### Specifying all the necessary paths of a dataset

You can use this if there is no existing pipeline run output yet for a pipeline,
or if you want to manually specify the dataset path (set the paths for `-r`,
`-i`, `-t`, `-a`, `-p` to your target dataset location).

Remember to change the bind mount paths as appropriate for your system
(specified by `-v`).

```shell
docker run \
    --rm \
    -v /home/foo/d3m:/mnt/d3m \
    registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9 \
    python3 -m d3m \
        runtime \
        fit-score \
            -r /mnt/d3m/datasets/seed_datasets_current/185_baseball/185_baseball_problem/problemDoc.json \
            -i /mnt/d3m/datasets/seed_datasets_current/185_baseball/TRAIN/dataset_TRAIN/datasetDoc.json \
            -t /mnt/d3m/datasets/seed_datasets_current/185_baseball/TEST/dataset_TEST/datasetDoc.json \
            -a /mnt/d3m/datasets/seed_datasets_current/185_baseball/SCORE/dataset_TEST/datasetDoc.json \
            -p /mnt/d3m/primitives/v2020.1.9/JPL/d3m.primitives.classification.logistic_regression.SKlearn/2019.11.13/pipelines/862df0a2-2f87-450d-a6bd-24e9269a8ba6.json \
            -o /mnt/d3m/pipeline-outputs/predictions.csv \
            -O /mnt/d3m/pipeline-outputs/run.yml
```

The score is displayed after the pipeline run. The output predictions will be
stored on the path specified by `-o`, and information about the pipeline run is
stored in the path specified by `-O`.

Again, you can use the `-h` flag on `fit-score` to access the help string and
read about the different arguments, as described earlier.

If you get a python error that complains about missing columns, or something
that looks like this:

```
ValueError: Mismatch between column name in data 'version https://git-lfs.github.com/spec/v1' and column name in metadata 'd3mIndex'.
```

Chances are that the `185_baseball` dataset has not yet been downloaded through
git LFS. See the [previous subsection](#preparing-the-dataset) for details on
how to verify and do this.

### Using a pipeline run output file

Instead of specifying all the specific dataset paths, you can also use an
existing pipeline run output to essentially "re-run" a previous run of the
pipeline.

We first have to decompress the gzipped pipeline run output that comes with the
sklearn primitive:

```shell
gzip -d /home/foo/d3m/primitives/v2020.1.9/JPL/d3m.primitives.classification.logistic_regression.SKlearn/2019.11.13/pipeline_runs/pipeline_run.yml.gz
```

With that, we can now do this:

```shell
docker run \
    --rm \
    -v /home/foo/d3m:/mnt/d3m \
    registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9 \
    python3 -m d3m \
        -p /mnt/d3m/primitives/v2020.1.9/JPL/d3m.primitives.classification.logistic_regression.SKlearn/2019.11.13/pipelines \
        runtime \
            -d /mnt/d3m/datasets/seed_datasets_current/185_baseball \
        fit-score \
            -u /mnt/d3m/primitives/v2020.1.9/JPL/d3m.primitives.classification.logistic_regression.SKlearn/2019.11.13/pipeline_runs/pipeline_run.yml \
            -o /mnt/d3m/pipeline-outputs/predictions.csv \
            -O /mnt/d3m/pipeline-outputs/run.yml
```

In this case, `-u` is the pipeline run output file that this pipeline will
re-run, and `-O` is the new pipeline run output file that will be generated.

Note that if you choose `fit-score` for the d3m runtime option, the pipeline
actually runs in two phases: fit, and produce. You can verify this by searching
for `phase` in the pipeline run output file.

Lastly, if you want to run multiple commands in the docker container, simply
chain your commands with `&&` and wrap them double quotes (`"`) for `bash -c`.
As an example:

```shell
docker run \
    --rm \
    -v /home/foo/d3m:/mnt/d3m \
    registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9 \
    /bin/bash -c \
        "python3 -m d3m \
            -p /mnt/d3m/primitives/v2020.1.9/JPL/d3m.primitives.classification.logistic_regression.SKlearn/2019.11.13/pipelines \
            runtime \
                -d /mnt/d3m/datasets/seed_datasets_current/185_baseball \
            fit-score \
                -u /mnt/d3m/primitives/v2020.1.9/JPL/d3m.primitives.classification.logistic_regression.SKlearn/2019.11.13/pipeline_runs/pipeline_run.yml \
                -o /mnt/d3m/pipeline-outputs/predictions.csv \
                -O /mnt/d3m/pipeline-outputs/run.yml && \
        head /mnt/d3m/pipeline-outputs/predictions.csv"
```

[back to top](#table-of-contents)

## Writing a new primitive

Let's now try to write a very simple new primitive - one that simply passes
whatever input data it receives from the previous step to the next step in the
pipeline. Let's call this primitive "Passthrough".

We will use this [skeleton primitive repo](https://github.com/tonyjo/d3m_test)
(called `d3m_test` at the time of this writing) as a starting point for this
exercise. A d3m primitive repo does not have to follow the exact same directory
structure as this, but this is a good structure to start with, at least.

Alternatively, you can also use the [test
primitives](https://gitlab.com/datadrivendiscovery/tests-data/tree/master/primitives)
as a model/starting point. `test_primitives/null.py` is essentially the same
primitive that we are trying to write.

### Primitive source code

In the `d3m_test` directory, open
`samplePrimitives/samplePrimitive1/input_to_output.py`. The first important
thing to change here is the primitive metadata, which are the first objects
defined under the `InputToOutput` class. Modify the following fields (unless
otherwise noted, the values you put in must be strings):

- `__author__`: This is usually your team/organization's name. For this
  exercise, just write `"MyTeam"` here.
- `id`: The primitive's UUID v4 number/identifier. To generate one, you can run
  simply run this simple inline Python command:

  ```shell
  python3 -c "import uuid; print(uuid.uuid4())"
  ```

- `version`: You can use semantic versioning for this or another style of
  versioning. Write `"0.1.0"` for this exercise.
- `name`: The primitive's name. Write `"Passthrough Primitive"` for this
  exercise.
- `description`: A short description of the primitive. Write `"A primitive which
  directly outputs the input"` for this exercise.
- `python_path`: This usually follows this format:

  ```
  d3m.primitives.<primitive family>.<primitive name>.<author/team name>
  ```
  
  Primitive families can be found in the [d3m metadata
  page](https://metadata.datadrivendiscovery.org/v2020.1.9/?definitions#definitions.primitive_family),
  and primitive names can be found in the [d3m core source
  code](https://gitlab.com/datadrivendiscovery/d3m/blob/devel/d3m/metadata/primitive_names.py).
  
  For this exercise, write
  `"d3m.primitives.data_preprocessing.do_nothing.MyTeam"`.

- `primitive_family`: This is the same as described above, but it is ideal to
  put the python path of the primitive family here instead of just the primitive
  family name by itself. Add this import statement (if not there already):
  
  ```python
  from d3m.metadata import base as metadata_base
  ```
  
  Then write `metadata_base.PrimitiveFamily.DATA_PREPROCESSING` (as an enum, not
  a string, so do not put quotation marks) as the value of this field.
  
- `algorithm_types`: Algorithm type(s) that the primitive implements. This can
  be multiple values in an array. Values can be chosen from the [d3m metadata
  page](https://metadata.datadrivendiscovery.org/v2020.1.9/?definitions#definitions.algorithm_types)
  as well, but it's ideal to write the full python path here as well. Write
  `[metadata_base.PrimitiveAlgorithmType.DATA_CONVERSION,]` here for this
  exercise (as a list that contains one element, not a string).
  
- `source`: General info about the author of this primitive. `name` is usually
  the name of the person or the team that wrote this primitive. `contact` is a
  mailto URI to the email address of whoever one should contact about this
  primitive. `uris` are usually the git clone URL of the repo, and you can also
  add the URL of the source file of this primitive.
  
  Write these for the exercise (the git repo doesn't exist, but pretend that it
  exists for now:
  
  ```python
  "name": "My Name",
  "contact": "mailto:myname@myteam.com",
  "uris": ['https://gitlab.com/myteam/d3m-myteam-primitives.git']
  ```
  
- `keywords`: Key words for what this primitive does. Write `["passthrough"]`.
- `installation`: Information about how to install this primitive. Add these
  import statements first:

  ```python
  from d3m import utils
  import os
  ```

  Then replace the `installation` entry with this:

  ```python
  "installation": [{
      "type": metadata_base.PrimitiveInstallationType.PIP,
      "package_uri": "git+https://gitlab.com/myteam/d3m-myteam-primitives@{git_commit}#egg=myteam_primitives".format(
          git_commit=utils.current_git_commit(os.path.dirname(__file__))),
      }],
  ```
  
  In general, for your own actual primitives, you might only need to substitute
  the git repo URL here as well as the python egg name.
  
Next, let's take a look at the `produce` method. You can see that it simply
makes a new dataframe out of the input data, and returns it as the output. To
see for ourselves though that our primitive (and thus this `produce` method)
gets called during the pipeline run, let's add a log statement here. The
`produce` method should now look something like this:

```python
def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
    self.logger.warning('Hi, InputToOutput.produce was called!')
    return base.CallResult(value=inputs)
```

### setup.py

Next, we fill in the necessary information in `setup.py` so that `pip` can
correctly install our primitive on our local d3m environment. Open `setup.py`
(in the project root), and fill in the `name`, `version`, etc. There are three
fields that we want to mainly look at:

- `packages`: This is an array of the python packages that this primitive repo
  contains. It should at least look like this:

  ```python
  packages=[
            'samplePrimitives',
            'samplePrimitives.samplePrimitive1',
           ],
  ```
  
- `install_requires`: This is an array of the python package dependencies of the
  primitives contained in this repo. Our primitive needs nothing except the d3m
  core package (and the `common-primitives` package too for testing, but this is
  not a package dependency), so write this as the value of this field: `['d3m']`
  
- `entry_points`: This is how the d3m runtime maps your primitives' d3m python
  paths to the your repo's local python paths. For this exercise, it should look
  like this:
  
  ```python
  entry_points={
      'd3m.primitives': [
          'data_preprocessing.do_nothing.MyTeam = samplePrimitives.samplePrimitive1:InputToOutput',
      ],
  }
  ```
  
That's it for this file. Briefly review it for any possible syntax errors.

### Primitive unit tests

Let's now make a python test for this primitive, which in this case will just
assert whether the input dataframe to the primitive equals the output dataframe.
Make a new file called `test_input_to_output.py` inside
`samplePrimitives/samplePrimitive1` (the same directory as `input_to_output.py`),
and write this as its contents:

```python
import unittest
import os

from d3m import container
from common_primitives import dataset_to_dataframe
from input_to_output import InputToOutput


class InputToOutputTestCase(unittest.TestCase):
    def test_output_equals_input(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'timeseries_dataset_1', 'datasetDoc.json'))

        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        dataframe_hyperparams_class = dataset_to_dataframe.DatasetToDataFramePrimitive.metadata.get_hyperparams()
        dataframe_primitive = dataset_to_dataframe.DatasetToDataFramePrimitive(hyperparams=dataframe_hyperparams_class.defaults())
        dataframe = dataframe_primitive.produce(inputs=dataset).value

        i2o_hyperparams_class = InputToOutput.metadata.get_hyperparams()
        i2o_primitive = InputToOutput(hyperparams=dataframe_hyperparams_class.defaults())
        output = i2o_primitive.produce(inputs=dataframe).value

        self.assertTrue(output.equals(dataframe))


if __name__ == '__main__':
    unittest.main()
```

For the dataset that this test uses, git clone the d3m `tests-data` repository
(see the Public Code link above) in a directory outside `d3m_test`, and copy the
`datasets/timeseries_dataset_1` directory to the same directory where
`test_input_to_output.py` is located. Then let's install this new primitive to
the Docker image's d3m environment, and run this test using the command below:

```shell
docker run \
    --rm \
    -v /home/foo/d3m:/mnt/d3m \
    registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9 \
    /bin/bash -c \
        "pip3 install -e /mnt/d3m/d3m_test && \
        cd /mnt/d3m/d3m_test/samplePrimitives/samplePrimitive1 && \
        python3 test_input_to_output.py"
```

You should see a log statement like this, as well as the python unittest pass
message:

```
Hi, InputToOutput.produce was called!
.
----------------------------------------------------------------------
Ran 1 test in 0.011s

```

### Using this primitive in a pipeline

Having seen the primitive test pass, we can now confidently include this
primitive in a pipeline. Let's take the same pipeline that we ran in the
previous section (the sklearn logistic regression's sample pipeline), and add
a step using this primitive. 

From the root directory of the project, create these directories:
`pipelines/data_preprocessing.do_nothing.MyTeam`. Then, from the d3m
`primitives` repo, copy the JSON pipeline description file from
`v2020.1.9/JPL/d3m.primitives.classification.logistic_regression.SKlearn/2019.11.13/pipelines`
into the directory we just created. Open this file, and replace the `id`
(generate another UUID v4 number using the inline python command earlier,
different from the primitive `id`), as well as the created timestamp using this
inline python command (add `Z` at the end of the generated timestamp):

```
python3 -c "import time; import datetime; \
print(datetime.datetime.fromtimestamp(time.time()).isoformat())"
```

You can rename the json file too using the pipeline `id`.

Next, change the output step number (shown below, `"steps.4.produce"`) to be one
more than the current number (at the time of this writing, it is currently `4`,
so in this case, change it to `5`):

```json
"outputs": [
  {
    "data": "steps.5.produce",
    "name": "output predictions"
  }
],
```

Then, find the step that contains the
`d3m.primitives.classification.logistic_regression.SKlearn` primitive (search
for this string in the file), and right above it, add the following JSON object.
Remember to change `primitive/id` to the Passthrough primitive's id that you
generated in the earlier "[Primitive source code](#primitive-source-code)"
subsection.

```json
{
  "type": "PRIMITIVE",
  "primitive": {
    "id": "c5ff8a75-2a13-4246-95a7-1467709ac014",
    "version": "0.1.0",
    "python_path": "d3m.primitives.data_preprocessing.do_nothing.MyTeam",
    "name": "Passthrough primitive"
  },
  "arguments": {
    "inputs": {
      "type": "CONTAINER",
      "data": "steps.2.produce"
    }
  },
  "outputs": [
    {
      "id": "produce"
    }
  ]
},
```

Make sure that the step number (`"steps.N.produce"`) in `arguments/inputs/data` is
correct (one greater than the previous step and one less than the next step). Do
this as well for the succeeding steps, with the following caveats:

- For `d3m.primitives.classification.logistic_regression.SKlearn`, increment the
  step number both for `arguments/inputs/data` and `arguments/outputs/data` (at
  the time of this writing, the number should be changed to `3`).
- For `d3m.primitives.data_transformation.construct_predictions.Common`,
  increment the step number for `arguments/outputs/data` (at the time of this
  writing, the number should be changed to `4`), but do not change the one for
  `arguments/reference/data` (the value should stay as `"steps.0.produce"`)
  
Generally, you can also programatically generate a pipeline, as described in the
[d3m
documentation](https://docs.datadrivendiscovery.org/v2020.1.9/pipeline.html#pipeline-description-example).
  
Now we can finally run this pipeline that uses our new primitive. In the command
below, modify the pipeline JSON filename in the `-p` argument to match the
filename of your pipeline file (if you changed it to the new pipeline id that
you generated).

``` shell
docker run \
    --rm \
    -v /home/foo/d3m:/mnt/d3m \
    registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9 \
    /bin/bash -c \
        "pip install -e /mnt/d3m/d3m_test && \
        python3 -m d3m \
            runtime \
            fit-score \
                -r /mnt/d3m/datasets/seed_datasets_current/185_baseball/185_baseball_problem/problemDoc.json \
                -i /mnt/d3m/datasets/seed_datasets_current/185_baseball/TRAIN/dataset_TRAIN/datasetDoc.json \
                -t /mnt/d3m/datasets/seed_datasets_current/185_baseball/TEST/dataset_TEST/datasetDoc.json \
                -a /mnt/d3m/datasets/seed_datasets_current/185_baseball/SCORE/dataset_TEST/datasetDoc.json \
                -p /mnt/d3m/d3m_test/pipelines/data_preprocessing.do_nothing.MyTeam/0f290525-3fec-44f7-ab93-bd778747b91e.json \
                -o /mnt/d3m/pipeline-outputs/predictions_new.csv \
                -O /mnt/d3m/pipeline-outputs/run_new.yml"
```

In the output messages, you should see the log statement as a warning, before
the score is shown (similar to the text below):

```
...
WARNING:d3m.primitives.data_preprocessing.do_nothing.MyTeam:Hi, InputToOutput.produce was called!
...
metric,value,normalized,randomSeed
F1_MACRO,0.31696136214800263,0.31696136214800263,0
```

Verify that the old and new `predictions.csv` in `pipeline-outputs` are the same
(you can use `diff`), as well as the scores in the old and new `run.yml` files
(search for `scores` in the files).

[back to top](#table-of-contents)

## Beyond this tutorial

Congratulations! You just built your own primitive and you were able to use it
in a d3m pipeline!

Normally, when you build your own primitives, you would proceed to validating
the primitives to be included in the d3m index of all known primitives. See the
[`primitives` repo
README](https://gitlab.com/datadrivendiscovery/primitives#adding-a-primitive) on
details on how to do this.

Also, go back to the ["Important links"](#important-links) section above and
take a look at the actual D3M documentation and other links.

The next section covers writing your own primitives and pipelines in more
detail. Use the following as a guide when making your own actual primitives.

[back to top](#table-of-contents)

# Quick reference to writing your own primitives and pipelines

## TA1 Primitive Development - 1: Primitive

Before building any primitives, it is important to set up a Github Project
repository as shown in examples below:
 - https://github.com/tonyjo/d3m_test

**Note:** All the explainations below will be based on the
[d3m_test](https://github.com/tonyjo/d3m_test) setup.

### Overview of building a primitive

1. When bulding a primitive, it is important to recognize what kind of
primitive, based on the available
[primitive_interfaces](https://gitlab.com/datadrivendiscovery/d3m/tree/devel/d3m/primitive_interfaces).
2. The next step is to identify the input and output
   [container](https://gitlab.com/datadrivendiscovery/d3m/tree/devel/d3m/container)
   types.
3. Setup
   [metadata](https://gitlab.com/datadrivendiscovery/d3m/tree/devel/d3m/metadata)
   for each primitive.
4. Write a Unit Test to verify the primitive functions.
5. Generate the `json` file description for the primitive.


#### 1. Primitive Kind
There are a variety of primitve interfaces available. As an example, just to
have feature extraction without any fitting required, meaning to produce useful
outputs (features) from inputs (data) directly, ``TransformerPrimitiveBase``
from `` transformer.py``can be used.

Each primitives can have it's own [hyperparameters](https://gitlab.com/datadrivendiscovery/d3m/blob/devel/d3m/metadata/hyperparams.py). Some example types are:
```
Constant, UniformBool, UniformInt, Choice, List
```
Also, each hyperparameters should be defined as one of the four [semantic types](https://docs.datadrivendiscovery.org/v2020.1.9/metadata.html#hyper-parameters):
```
https://metadata.datadrivendiscovery.org/types/TuningParameter
https://metadata.datadrivendiscovery.org/types/ControlParameter
https://metadata.datadrivendiscovery.org/types/ResourcesUseParameter
https://metadata.datadrivendiscovery.org/types/MetafeatureParameter
```

**Sample Setup**
```python
from d3m.primitive_interfaces import base, transformer
from d3m.metadata import base as metadata_base, hyperparams

__all__ = ('SampleTransform',)

class Hyperparams(hyperparams.Hyperparams):
  """
  docstrings
  """
  learning_rate = hyperparams.Uniform(lower=0.0, upper=1.0, default=0.001, semantic_types=[
      'https://metadata.datadrivendiscovery.org/types/TuningParameter'
  ])
  clusters = hyperparams.UniformInt(lower=1, upper=100, default=10, semantic_types=[
      'https://metadata.datadrivendiscovery.org/types/TuningParameter'
  ])

class SampleTransform(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
  """
  docstrings
  """
  def __init__(self, *, hyperparams: Hyperparams, volumes: typing.Union[typing.Dict[str, str], None]=None):
    super().__init__(hyperparams=hyperparams, volumes=volumes)
    self.hyperparams = hyperparams

  def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
    pass
```

#### 2. Input/Output type
The acceptable input/outputs must be pre-defined. D3M supports a variety of
standard IO's such as:
- DataFrame :`pandas.DataFrame`
- ndarray :`numpy.ndarray`
- matrix :`numpy.matrix`
- List :`list`

**Sample Setup**

```python
from d3m import container

Inputs  = container.DataFrame
Outputs = container.DataFrame


class SampleTransform(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    docstrings
    """
    __author__ = <Your-Name>
    metadata   = ...

    def __init__(self, *, hyperparams: Hyperparams, volumes: typing.Union[typing.Dict[str, str], None]=None):
        super().__init__(hyperparams=hyperparams, volumes=volumes)
       ....
    ...
```

#### 3. Metadata

It is very crucial to set up the metadata for the primitive properly. D3M uses
the primitive's metadata to index and run TA-2 systems.

Below is a description of core metadata setup required for each primitive.

**Sample Setup**

```python
from d3m.primitive_interfaces import base, transformer
from d3m.metadata import base as metadata_base, hyperparams

__all__ = ('SampleTransform',)

class SampleTransform(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    docstrings
    """
    __author__ = <Your-Name>
    metadata = metadata_base.PrimitiveMetadata({
        'id': <Unique-ID, generated using UUID>,
        'version': <Primitive-development-version>,
        'name': <Primitive-Name>,
        'python_path': 'd3m.primitives.<>.<>' # Must match path in setup.py,
        'source': {
            'name': <Project-maintainer-name>,
            'uris': [<GitHub-link-to-project>],
            'contact': 'mailto:<Author E-Mail>'
        },
        'installation': [{
            'type': metadata_base.PrimitiveInstallationType.PIP,

            'package_uri': 'git+<GitHub-link-to-project>@{git_commit}#egg=<Project_Name>'.format(
                git_commit=d3m_utils.current_git_commit(os.path.dirname(__file__)),
            ),
        }],
        'algorithm_types': [
            metadata_base.PrimitiveAlgorithmType.<Choose-the-algorithm-type-that-best-describes-the-primitive>,
            # Check https://metadata.datadrivendiscovery.org/devel/?definitions#definitions.algorithm_types for all available algorithm types.
        ],
        'primitive_family': metadata_base.PrimitiveFamily.<Choose-the-primitive-familty-that-closely-associates-to-the-primitive>
        # Check https://metadata.datadrivendiscovery.org/devel/?definitions#definitions.primitive_family for all available primitive family types.
    })

    def __init__(self, *, hyperparams: Hyperparams, volumes: typing.Union[typing.Dict[str, str], None]=None):
      ....
```

#### 4. Unit Test
Once the primitives are constructed, unit testing must be done to see if the
primitive works as intended.

**Sample Setup**

```python
import os
import unittest

from d3m.container.dataset import Dataset
from d3m.metadata import base as metadata_base
from common_primitives import dataset_to_dataframe

from sample_primitive import SampleTransform

class TestSampleTransform(unittest.TestCase):
  """
  docstrings
  """
  def test():
    # Load Some dataset,
    # Datasets can be obtained from: https://gitlab.datadrivendiscovery.org/d3m/datasets
    base_path = '.../datasets/seed_datasets_current/'
    dataset_doc_path = os.path.join(base_path, '38_sick_dataset', 'datasetDoc.json')
    dataset = Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))
    dataframe_hyperparams_class = dataset_to_dataframe.DatasetToDataFramePrimitive.metadata.get_hyperparams()
    dataframe_primitive = dataset_to_dataframe.DatasetToDataFramePrimitive(hyperparams=dataframe_hyperparams_class.defaults())
    dataframe = dataframe_primitive.produce(inputs=dataset)

    # Call Primitive
    hyperparams_class = SampleTransform.metadata.get_hyperparams()
    primitive  = SampleTransform(hyperparams=hyperparams_class.defaults(), volumes=all_volumes)
    test_out   = primitive.produce(inputs=dataframe.value)

    # Write Checks to make sure that the output (type and shape) is whats Expected
    assert()

    ...

if __name__ == '__main__':
  unittest.main()

```

It is recommended to do the testing inside the D3M docker container:

```shell
docker run  -v ../local/path/d3m_test:/d3m_test
            -i -t registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9\
            /bin/bash

cd /d3m_test/samplePrimitive1

python3 primitive_name_test.py
```

#### 5. Primitive `json` file description
Once primitive is constructed and unit testing is successfully completed, the
final step in building a primitve is to generate the `json` file description
which will be indexed and used by D3M.

```shell
docker run  --rm \
            -v ../local/path/d3m_test:/d3m_test\
            -i -t registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9\
            /bin/bash

cd /d3m_test

pip3 install -e .

cd /samplePrimitive1

python3 -m d3m index describe -i 4 <primitive_name>
```

or a [python file](https://github.com/tonyjo/d3m_test/blob/master/samplePrimitives/generate-primitive-json.py) can be written to generate the `json` file description as well. This would be more convenient when having to manage multiple primitives. In this case, generating the `json` primitive is shown as follows:
```shell
docker run  --rm \
            -v ../local/path/d3m_test:/d3m_test
            -i -t registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9\
            /bin/bash

cd /d3m_test

pip3 install -e .

python3 generate-primitive-json.py --args..
```

[back to top](#table-of-contents)

## TA1 Primitive Development - 2: Pipeline

After building any primitives, it has to put into a pipeline and validated in
order to integrate into the `D3M` primitives:

**Note:** All the explainations below will be based on the
[d3m_test](https://github.com/tonyjo/d3m_test) setup.

The esstential elements in building pipelines is:
```
 Dataset loader -> Dataset Parser -> Data Cleaner (In necessary) -> Classifier/Regression -> Output
```
#### 1. Import all the necessary packages

Packages can be directly imported from `d3m` or common primitives. Import the
necessary packages and the custom primitive.

```python
from d3m import index
from d3m.metadata import base as metadata_base
from d3m.metadata.base import Context, ArgumentType
from d3m.metadata.pipeline import Pipeline, PrimitiveStep

import d3m.primitives.data_cleaning.imputer as Imputer
import d3m.primitives.classification.random_forest as RF

# Testing primitive
from samplePrimitives.samplePrimitive1.input_to_output import InputToOutput

```

#### 2. Primitive step construction

Define primitive for each stage/step of the pipeline along with:

-`Inputs`

-`outputs`

-`hyperparameters`



**Sample Setup**

```python

step_6 = PrimitiveStep(primitive=index.get_primitive('d3m.primitives.classification.decision_tree.SKlearn'))
step_6.add_argument(name='inputs',  argument_type=ArgumentType.CONTAINER,  data_reference='steps.5.produce')
step_6.add_argument(name='outputs', argument_type=ArgumentType.CONTAINER, data_reference=targets)
step_6.add_output('produce')
pipeline.add_step(step_6)
```

#### 3. Run and Validate `pipeline`

Once pipeline is constructed, `json` file is generated and using `python3 -m d3m
runtime` to validate the pipeline.

```shell

sudo docker run --rm\
                -v ../local/path/d3m_test:/d3m_test\
                -i -t registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9 /bin/bash\
                -c "cd /d3m_test;\
                    pip3 install -e .;\
                    cd pipelines;\
                    python3 primitive1_pipeline.py;\
                    python3 -m d3m runtime fit-produce \
                            -p pipeline.json \
                            -r /d3m_test/datasets/seed_datasets_current/38_sick/TRAIN/problem_TRAIN/problemDoc.json \
                            -i /d3m_test/datasets/seed_datasets_current/38_sick/TRAIN/dataset_TRAIN/datasetDoc.json \
                            -t /d3m_test/datasets/seed_datasets_current/38_sick/TEST/dataset_TEST/datasetDoc.json \
                            -o 38_sick_results.csv \
                            -O pipeline_run.yml;\
                    exit"

```

[back to top](#table-of-contents)

# Contributors
<img src="images/ubc.png" width="63" height="86"> <img src="images/arrayfire.png" width="100" height="100">
